var mapSW = [0, 4096],
  mapNE = [4096, 0];
// Declare map Object
var map = L.map("map").setView([0, 0], 2);

var zoomHome = L.Control.zoomHome();
zoomHome.addTo(map);

/*
 *
 *  Markers & Content are matched by:
 *
 *   ALT: Image on the Map
 *   ID: The title of the muscle - sidebar
 *   CLASS: The Description of the muscle - sidebar2
 *
 *
 */

// Reference the Tiles
if (window.screen.width < 768) {
  L.tileLayer("maps-posterior/{z}/{x}/{y}.png", {
    minZoom: 0,
    maxZoom: 4,
    continousWorld: false,
    noWrap: true,
    crs: L.CRS.Simple,
  }).addTo(map);

  //max granici na mapata
  map.setMaxBounds(
    new L.LatLngBounds(
      map.unproject(mapSW, map.getMaxZoom()),
      map.unproject(mapNE, map.getMaxZoom())
    )
  );
} else {
  L.tileLayer("maps-posterior/{z}/{x}/{y}.png", {
    minZoom: 0,
    maxZoom: 4,
    continousWorld: false,
    noWrap: true,
    crs: L.CRS.Simple,
  }).addTo(map);

  //max granici na mapata
  map.setMaxBounds(
    new L.LatLngBounds(
      map.unproject(mapSW, map.getMaxZoom()),
      map.unproject(mapNE, map.getMaxZoom())
    )
  );
}

//max granici na mapata
map.setMaxBounds(
  new L.LatLngBounds(
    map.unproject(mapSW, map.getMaxZoom()),
    map.unproject(mapNE, map.getMaxZoom())
  )
);

document.getElementsByClassName(
  "leaflet-control-attribution"
)[0].style.display = "none";

//   console.log("ghjghj!");

// Icons
var myIconRed = L.icon({
  iconUrl: "scripts/leafletJS/images/leaf-red.png",
  iconSize: [38, 95],
  iconAnchor: [22, 94],
  popupAnchor: [-3, -76],
  shadowUrl: "scripts/leafletJS/images/leaf-shadow.png",
  shadowSize: [41, 41],
  shadowAnchor: [5, 40],
});
//L.marker([50.505, 30.57], {icon: myIconRed}).addTo(map);
var myIconGreen = L.icon({
  iconUrl: "scripts/leafletJS/images/leaf-green.png",
  iconSize: [38, 95],
  iconAnchor: [22, 94],
  popupAnchor: [-3, -76],
  shadowUrl: "scripts/leafletJS/images/leaf-shadow.png",
  shadowSize: [41, 41],
  shadowAnchor: [5, 40],
});
var markerZoom = L.icon({
  iconUrl: "img/marker-mine.svg",
  iconSize: [28],
  iconAnchor: [14, 14],
  // popupAnchor: [-3, -76],
  //  shadowUrl: 'scripts/leafletJS/images/leaf-shadow.png',
  // shadowSize: [41, 41],
  // shadowAnchor: [5, 40]
});
var markerZoomHover = L.icon({
  iconUrl: "img/marker-mine-hover.svg",
  iconSize: [28],
  iconAnchor: [14, 14],
  // popupAnchor: [-3, -76],
  //  shadowUrl: 'scripts/leafletJS/images/leaf-shadow.png',
  // shadowSize: [41, 41],
  // shadowAnchor: [5, 40]
});
var markerZoomSVG = L.icon({
  iconUrl: "img/search1.svg",
  iconSize: [25, 25],
  iconAnchor: [14, 14],
});
const fontAwesomeIcon = L.divIcon({
  html: '<i class="fas fa-search" style="color:green"></i>',
  iconSize: [40, 40],
  className: "myDivIcon",
});

// Markers and Popups
var refMarker = L.marker([2, -70], {
  draggable: true,
});
refMarker.bindPopup("");

// Pixels
var marker_center = L.marker(map.unproject([2048, 2048], map.getMaxZoom()));
marker_center.bindPopup("markerCenter").openPopup();

refMarker.on("dragend", function (e) {
  refMarker
    .getPopup()
    .setContent(
      // "Clicked " +
      refMarker.getLatLng().toString() +
        "<br />" +
        map.project(refMarker.getLatLng(), map.getMaxZoom().toString())
    )
    .openOn(map);
});
//refMarker.addTo(map);

var randomNumber = 0;
var addressPointsHead = [
  [57.515823, -5.800781, "markerHead" + randomNumber++], // head 1
  [58.263287, 18.457031, "markerHead" + randomNumber++], // head 2
  [52.589701, -14.238281, "markerHead" + randomNumber++], // neck 1
  [46.920255, -17.402344, "markerHead" + randomNumber++], // neck 2
  [48.922499, 8.085938, "markerHead" + randomNumber++], // neck 3
  [45.089036, -30.9375, "markerHead" + randomNumber++], // levo ramo marker 1
  [42.940339, -25.664063, "markerHead" + randomNumber++], // levo ramo marker 2
  [24.046464, 31.816406, "markerHead" + randomNumber++], // levo raka marker 1
  [38.548165, 16.523438, "markerHead" + randomNumber++], // levo raka marker 2
  [28.767659, 11.777344, "markerHead" + randomNumber++], // levo raka marker 3
  [11.695273, -12.304688, "markerHead" + randomNumber++], //  levo dlanka marker 1
  [1.757537, 8.4375, "markerHead" + randomNumber++], //   gradi sredina
  [-9.968851, 9.316406, "markerHead" + randomNumber++], // gradi desno
  [-17.644022, -8.789063, "markerHead" + randomNumber++], //   desno biceps 1
  [-19.808054, -2.988281, "markerHead" + randomNumber++], //  desno biceps 2
  [-30.448674, -4.394531, "markerHead" + randomNumber++], //levo noga 1
  [-31.952162, 10.019531, "markerHead" + randomNumber++], // levo noga 2
  [-37.160317, 10.722656, "markerHead" + randomNumber++], //levo noga 3
  [-29.22889, 13.886719, "markerHead" + randomNumber++], // levo noga 4
  [-34.307144, 14.414063, "markerHead" + randomNumber++], //   levo noga 5
  [-39.095963, 14.589844, "markerHead" + randomNumber++], //  desno noga 1
  [-29.688053, 18.632813, "markerHead" + randomNumber++], //  desno noga 2
  [-35.173808, 18.808594, "markerHead" + randomNumber++], //  desno noga 3
  [-52.48278, -16.699219, "markerHead" + randomNumber++], //  desno noga 4
  [-52.908902, -11.074219, "markerHead" + randomNumber++], //  desno noga 5
  [-56.848972, -17.050781, "markerHead" + randomNumber++], //  desno noga 6
  [-57.704147, -10.546875, "markerHead" + randomNumber++], //  desno noga 7
];

if (window.screen.width < 768) {
  var markersHead = L.markerClusterGroup({
    showCoverageOnHover: false,
    animate: true,
    spiderfyOnMaxZoom: false,
    disableClusteringAtZoom: 2,
    //maxClusterRadius:35
  });
} else {
  var markersHead = L.markerClusterGroup({
    showCoverageOnHover: false,
    animate: true,
    spiderfyOnMaxZoom: false,
    disableClusteringAtZoom: 3,
    //maxClusterRadius:35
  });
}

if (window.screen.width <= 768) {
  var allSidebarLinks = document.querySelectorAll('*[class^="markerHead"]');

  if (allSidebarLinks != undefined) {
    for (var i = 0; i < allSidebarLinks.length; i++) {
      currentSidebarLink = allSidebarLinks[i];

      var link_a = document.createElement("a");
      link_a.classList.add("btn");
      link_a.classList.add("btn-lg");
      link_a.classList.add("active");
      link_a.classList.add("kopce2");

      link_a.innerHTML = "View on Map >>";

      link_a.addEventListener("touch", function (e) {
        clickTheMarker(e.target.parentElement.className);
        setTimeout(() => {
          closeButton_mobile();
        }, 910);
      });
      link_a.onclick = function (e) {
        clickTheMarker(e.target.parentElement.className);
        setTimeout(() => {
          closeButton_mobile();
        }, 910);
      };

      currentSidebarLink.appendChild(link_a);
    }
  }
}

var allZooms = document.querySelectorAll(".leaflet-control-zoom");

if (allZooms != undefined) {
  allZooms[0].style.display = "none";
  // allZooms[1].style.display = "none";
}

window.onresize = () => {
  stickyImg();
};

window.onload = () => {
  stickyImg();
};

function stickyImg() {
  if (window.screen.width > 768) {
    var btnpos = document.querySelector(".btnpos");
    if (btnpos != undefined) {
      setTimeout(() => {
        var sidebarWidth = document.getElementById("sidebar");

        var width = sidebarWidth.offsetWidth + 20;
        btnpos.style.left = width + "px";
      }, 60);
    }
  }
}

function closeButton_mobile() {
  var sidebar = document.getElementById("sidebar");
  sidebar.classList.add("collapsed");

  var sidebar2 = document.getElementById("sidebar2");
  sidebar2.classList.add("collapsed");

  stickyImg();
}

function clickTheMarker(markerAlt) {
    //closeButton();
    if (window.screen.width <= 768) {
      zoomOutAfterHide();
  }

  //closeButton();
  //var centerMap = document.querySelectorAll(".fa-dot-circle");
  //if (centerMap != undefined) {
  //  centerMap[0].click();
  //}

  setTimeout(() => {
    var allClusters = document.querySelectorAll(".marker-cluster");
    if (allClusters.length != 0) {
    //  for (var h = 0; h < allClusters.length; h++) {
        var currentCluster = allClusters[5];
        currentCluster.click();
     // }
    }
    setTimeout(() => {
      var allMarkers = getImagesByAlt(markerAlt);
      var matchedMarker = allMarkers[0];

      if (matchedMarker != undefined) {
        matchedMarker.click();
        backToDefaultImgs();
        setTimeout(() => {
          matchedMarker.src = "img/marker-mine-hover.svg";
        }, 300);
        console.log("Marker found! \n");
      } else {
          console.log("No matching marker found on map!");
          location.reload();
      }
    }, 300);
  }, 600);
}

function backToDefaultImgs() {
  var allMarkers = document.querySelectorAll('*[alt^="markerHead"]');
  for (var l = 0; l < allMarkers.length; l++) {
    var currentMarker = allMarkers[l];
    currentMarker.src = "img/marker-mine.svg";
  }
}

function backToAllContent() {
  var sidebar = document.getElementById("sidebar");
  sidebar.classList.remove("collapsed");
  sidebar.style.display = "";

  var sidebar2 = document.getElementById("sidebar2");
  sidebar2.classList.add("collapsed");
  sidebar2.style.display = "none";

  var centerMap = document.querySelectorAll(".fa-dot-circle");
  if (centerMap != undefined) {
    centerMap[0].click();
  }
}

function closeButton() {
  var sidebar = document.getElementById("sidebar");
  sidebar.classList.add("collapsed");

  var sidebar2 = document.getElementById("sidebar2");
  sidebar2.classList.add("collapsed");

  var centerMap = document.querySelectorAll(".fa-dot-circle");
  if (centerMap != undefined) {
    centerMap[0].click();
  }
}

function clickZoom(e) {
  map.setView(e.target.getLatLng(), 4);
}

/*   Head and Neck -->  Region  */

if (window.screen.width > 768) {
  setTimeout(() => {
    var head = document.querySelector(".headRegion");
    head.addEventListener("mouseover", function (e) {
      this.style.stroke = "green";
      var text = document.getElementById("headRegion");
      text.classList.add("markers_title_hovered");
    });
    head.addEventListener("mouseout", function (e) {
      this.style.stroke = "rgba(255,255,0,0)";
      var text = document.getElementById("headRegion");
      text.classList.remove("markers_title_hovered");
    });

    var chest = document.querySelector(".chestRegion");
    chest.addEventListener("mouseover", function (e) {
      this.style.stroke = "green";
      var text = document.getElementById("chestRegion");
      text.classList.add("markers_title_hovered");
    });
    chest.addEventListener("mouseout", function (e) {
      this.style.stroke = "rgba(255,255,0,0)";
      var text = document.getElementById("chestRegion");
      text.classList.remove("markers_title_hovered");
    });

    var abdomen = document.querySelector(".abdRegion");
    abdomen.addEventListener("mouseover", function (e) {
      this.style.stroke = "green";
      var text = document.getElementById("abdRegion");
      text.classList.add("markers_title_hovered");
    });
    abdomen.addEventListener("mouseout", function (e) {
      this.style.stroke = "rgba(255,255,0,0)";
      var text = document.getElementById("abdRegion");
      text.classList.remove("markers_title_hovered");
    });

    var arms = document.querySelector(".armsRegion");
    arms.addEventListener("mouseover", function (e) {
      this.style.stroke = "green";
      var text = document.getElementById("armsRegion");
      text.classList.add("markers_title_hovered");
    });
    arms.addEventListener("mouseout", function (e) {
      this.style.stroke = "rgba(255,255,0,0)";
      var text = document.getElementById("armsRegion");
      text.classList.remove("markers_title_hovered");
    });

    var legs = document.querySelector(".legsRegion");
    legs.addEventListener("mouseover", function (e) {
      this.style.stroke = "green";
      var text = document.getElementById("legsRegion");
      text.classList.add("markers_title_hovered");
    });
    legs.addEventListener("mouseout", function (e) {
      this.style.stroke = "rgba(255,255,0,0)";
      var text = document.getElementById("legsRegion");
      text.classList.remove("markers_title_hovered");
    });
  }, 2000);
}

/*   Head and Neck -->  Region  ::  Click  */

var abdShown = 0;
var chestShown = 0;
var armsShown = 0;
var legsShown = 0;

window.onload = () => {
    hideAll();
};

function hideAll() {
    var allSidebarLinks = document.querySelectorAll('*[id^="markerHead"]');

    if (allSidebarLinks != undefined) {
        for (var lll in allSidebarLinks) {
            if (lll == "length") break;
            var link = allSidebarLinks[lll];

            link.hidden = true;
        }
    }

    // Refresh the stack :D

    if (abdShown > 5) location.reload();
    if (chestShown > 5) location.reload();
    if (armsShown > 5) location.reload();
    if (legsShown > 5) location.reload();
}

function zoomOutAfterHide() {
    setTimeout(() => {
        var lat = ["LatLng", "5.615986"];
        var lng = [" 0", ""];

        map.flyTo([lat[1], lng[0]], 2, {
            animate: true,
            duration: 0.3,
        });
    }, 100);
}

function clickRegion(regionNum) {
    switch (regionNum) {
        case "0":
            //hideAll();

            //if (headShown % 2 == 0) {
            //    showSidebarLinks(".head");        SEA PA ZA OVIE NEMET LINKOJ ..
            //    ++headShown;
            //}
            //else {
            //    zoomOutAfterHide();
            //    ++headShown;
            //}

            var lat = ["LatLng", "71.187754"];
            var lng = [" 0", ""];

            map.flyTo([lat[1], lng[0]], 4, {
                animate: true,
                duration: 0.3,
            });
            break;
        case "1":
            hideAll();

            if (chestShown % 2 == 0) {
                showSidebarLinks(".chest");
                ++chestShown;
            }
            else {
                zoomOutAfterHide();
                ++chestShown;
            }

            var lat = ["LatLng", "32.842674"];
            var lng = [" 0", ""];

            map.flyTo([lat[1], lng[0]], 4, {
                animate: true,
                duration: 0.3,
            });
            break;
        case "2":
            hideAll();

            if (abdShown % 2 == 0) {
                showSidebarLinks(".abdomen");
                ++abdShown;
            }
            else {
                zoomOutAfterHide();
                ++abdShown;
            }

            var lat = ["LatLng", "5.615986"];
            var lng = [" 0", ""];

            map.flyTo([lat[1], lng[0]], 4, {
                animate: true,
                duration: 0.3,
            });
            break;
        case "3":
            hideAll();

            if (armsShown % 2 == 0) {
                showSidebarLinks(".arms");
                ++armsShown;
            }
            else {
                zoomOutAfterHide();
                ++armsShown;
            }

            var lat = ["LatLng", "39.909736"];
            var lng = [" -28.125", ""];

            map.flyTo([lat[1], lng[0]], 4, {
                animate: true,
                duration: 0.3,
            });
            break;
        case "4":
            hideAll();

            if (legsShown % 2 == 0) {
                showSidebarLinks(".legs");
                ++legsShown;
            }
            else {
                zoomOutAfterHide();
                ++legsShown;
            }

            var lat = ["LatLng", "-37.439974"];
            var lng = [" 9.492188", ""];

            map.flyTo([lat[1], lng[0]], 4, {
                animate: true,
                duration: 0.3,
            });
            break;
        default:
    }
}

function showSidebarLinks(regionName) {
    var allLinks = document.querySelectorAll(regionName);

    if (allLinks != undefined) {
        for (var a in allLinks) {
            var link = allLinks[a];

            if (a == "length") break;

            link.hidden = false;
        }
    }
}

/*   Region -->  Head and Neck   */

function highlightRegion(regionName, toggle) {
  if (toggle == "show") {
    var region = document.querySelector("." + regionName);
    region.style.stroke = "green";
  } else if (toggle == "hide") {
    var region = document.querySelector("." + regionName);
    region.style.stroke = "rgba(255,255,0,0)";
  }
}

/*  Sidebar Links --> Markers    */

var allLinksOnsSidebar = document.querySelectorAll(".markers_title");

for (var z = 0; z < allLinksOnsSidebar.length; z++) {
  var currentLink = allLinksOnsSidebar[z];

  currentLink.addEventListener("touch", function (e) {
    clickTheMarker(e.target.id);
    console.log("Touch Event Works!");
  });
  currentLink.addEventListener("click", function (e) {
    clickTheMarker(e.target.id);
  });
  currentLink.addEventListener("mouseover", function (e) {
    var allMarkers = getImagesByAlt(e.target.id);
    var matchedMarker = allMarkers[0];

    if (matchedMarker != undefined) {
      matchedMarker.src = "img/marker-mine-hover.svg";
      matchedMarker.style.width = "28px";
      //matchedMarker.style.height = "28px";
    }
  });
  currentLink.addEventListener("mouseout", function (e) {
    var allMarkers = getImagesByAlt(e.target.id);
    var matchedMarker = allMarkers[0];

    if (matchedMarker != undefined) {
      matchedMarker.src = "img/marker-mine.svg";
      matchedMarker.style.width = "28px";
      // matchedMarker.style.height = "28px";
    }
  });
}

/*   Markers --> Sidebar Links  */

for (var i = 0; i < addressPointsHead.length; i++) {
  var a = addressPointsHead[i];
  var title = a[2];

  var marker = L.marker(new L.LatLng(a[0], a[1]), {
    alt: title,
    icon: markerZoom,
  })
    .on("mouseover", function (e) {
      var markerAlt = e.target.options.alt;
      var markerElement = document.getElementById(markerAlt);
      markerElement.classList.add("markers_title_hovered");
      e.target.setIcon(markerZoomHover);
    })
    .on("click", clickZoom)
    .on("mouseout", function (e) {
      var markerAlt = e.target.options.alt;
      var markerElement = document.getElementById(markerAlt);
      markerElement.classList.remove("markers_title_hovered");
      e.target.setIcon(markerZoom);
    })
    .on("click", function (e) {
      var sidebar = document.getElementById("sidebar");
      sidebar.classList.add("collapsed");
      sidebar.style.display = "none";
      var sidebar2 = document.getElementById("sidebar2");
      sidebar2.style.display = "";
      sidebar2.classList.remove("collapsed");

      var allMuscleDescriptions = document.querySelectorAll(
        '*[class^="markerHead"]'
      );
      var allMuscleDescriptions_Array = Array.from(allMuscleDescriptions);
      var markerAlt = e.target.options.alt;

      for (var l = 0; l < allMuscleDescriptions.length; l++) {
        var currentDesc = allMuscleDescriptions[l];

        if (currentDesc.classList.contains(markerAlt)) {
          currentDesc.style.display = "";
        } else {
          currentDesc.style.display = "none";
        }
      }

      //e.target._popup = false;
      e.target.setIcon(markerZoom);
    });
  //marker.bindPopup(title);
  markersHead.addLayer(marker);
}

function openSidebar(sidebar) {}

map.addLayer(markersHead);

//map.on("moveend", function (e) {
//  console.log("Hello world!");
//});

function getImagesByAlt(alt) {
  var allImages = document.getElementsByTagName("img");
  var images = [];
  for (var i = 0, len = allImages.length; i < len; ++i) {
    if (allImages[i].alt == alt) {
      images.push(allImages[i]);
    }
  }
  return images;
}

function hovbeerrrr() {
  var allParagraph = document.querySelectorAll("");

  for (var i in allParagraph) {
    var currentElement = allParagraph[i];
    currentElement.style.display = "";
  }
}

function hideFirstParagraph() {
  var allParagraph = document.querySelectorAll(".even");

  for (var i in allParagraph) {
    var currentElement = allParagraph[i];
    currentElement.style.display = "none";
  }

  var allParagraph = document.querySelector("even");
  //firstParagraph.style.display = "none";
}

//marker1.addTo(map);

// =============== PanTo center marker

/*


var markerPrv = L.marker([32.842674, -64.6875], {
title: "marker_1",
})
.addTo(map)
.bindPopup("Marker 1")
.on("click", clickZoom);

var marker2 = L.marker([43.834527, -37.265625], {
title: "marker_2",
})
.addTo(map)
.bindPopup("Marker 2")
.on("click", clickZoom);

var marker3 = L.marker([11.178402, -19.6875], {
title: "marker_3",
})
.addTo(map)
.bindPopup("Marker 3")
.on("click", clickZoom);

function clickZoom(e) {
map.setView(e.target.getLatLng(), 15);
}

//everything below here controls interaction from outside the map
var markers = [];
markers.push(markerPrv);
markers.push(marker2);
markers.push(marker3);



function markerFunction(id) {
for (var i in markers) {
  var markerID = markers[i].options.title;
  var position = markers[i].getLatLng();
  if (markerID == id) {
    map.setView(position, 4); // second ARG- zoom lveel
    //markers[i].openPopup();
  }
}
}

$("a").click(function () {
markerFunction($(this)[0].id);
});
*/
