var mapSW = [0, 4096],
    mapNE = [4096, 0];

    // Declare map Object 
var map = L.map('map').setView([0, 0], 2);



var zoomHome = L.Control.zoomHome();
zoomHome.addTo(map);


    // Reference the Tiles
L.tileLayer('maps/{z}/{x}/{y}.png', {
    minZoom: 0,
    maxZoom: 4,
    continousWorld: false,
    noWrap: true,
    crs: L.CRS.Simple,
}).addTo(map);

//max granici na mapata
map.setMaxBounds(new L.LatLngBounds(
    map.unproject(mapSW, map.getMaxZoom()),
    map.unproject(mapNE, map.getMaxZoom())
    ));
    
   

    // Icons
 var myIconRed = L.icon({
    iconUrl: 'scripts/leafletJS/images/leaf-red.png',
    iconSize: [38, 95],
    iconAnchor: [22, 94],
    popupAnchor: [-3, -76],
    shadowUrl: 'scripts/leafletJS/images/leaf-shadow.png',
    shadowSize: [41, 41],
    shadowAnchor: [5, 40]
});
//L.marker([50.505, 30.57], {icon: myIconRed}).addTo(map);
var myIconGreen = L.icon({
    iconUrl: 'scripts/leafletJS/images/leaf-green.png',
    iconSize: [38, 95],
    iconAnchor: [22, 94],
    popupAnchor: [-3, -76],
    shadowUrl: 'scripts/leafletJS/images/leaf-shadow.png',
    shadowSize: [41, 41],
    shadowAnchor: [5, 40]
});
var markerZoom = L.icon({
    iconUrl: 'img/marker-mine.png',
   // iconSize: [38, 95],
    iconAnchor: [14, 14],
   // popupAnchor: [-3, -76],
   //  shadowUrl: 'scripts/leafletJS/images/leaf-shadow.png',
   // shadowSize: [41, 41],
   // shadowAnchor: [5, 40]
});

    // Markers and Popups
var refMarker = L.marker([2, -70],{
    draggable: true,
}).addTo(map);
refMarker.bindPopup('');


    // Pixels
var marker_center = L.marker(map.unproject([2048, 2048], map.getMaxZoom())).addTo(map);
marker_center.bindPopup('markerCenter').openPopup();

refMarker.on('dragend', function(e) {
    refMarker.getPopup().setContent('Clicked ' + refMarker.getLatLng().toString() + '<br />'
    + 'Pixels ' + map.project(refMarker.getLatLng(), map.getMaxZoom().toString())).openOn(map);
});

    // Pixel markers 
var markerHead_1 = L.marker(map.unproject([2049, 800], map.getMaxZoom()),{icon: markerZoom})
    .bindPopup('<b>markerHead_1');
var markerHead_2 = L.marker(map.unproject([2052, 864], map.getMaxZoom()),{icon: markerZoom})
    .bindPopup('<b>markerHead_2');
var markerHead_3 = L.marker(map.unproject([2159, 922], map.getMaxZoom()),{icon: markerZoom})
    .bindPopup('<b>markerHead_3');
 var markerHead_4 = L.marker(map.unproject([2122, 942], map.getMaxZoom()),{icon: markerZoom})
     .bindPopup('<b>markerHead_4');
 var markerHead_5 = L.marker(map.unproject([2050, 988], map.getMaxZoom()),{icon: markerZoom})
     .bindPopup('<b>markerHead_5');
 var markerHead_6 = L.marker(map.unproject([2018, 1028], map.getMaxZoom()),{icon: markerZoom})
     .bindPopup('<b>markerHead_6');
 var markerHead_7 = L.marker(map.unproject([1994, 1039], map.getMaxZoom()),{icon: markerZoom})
     .bindPopup('<b>markerHead_7');
 var markerHead_8 = L.marker(map.unproject([1968, 1065], map.getMaxZoom()),{icon: markerZoom})
     .bindPopup('<b>markerHead_8');
 var markerHead_9 = L.marker(map.unproject([2015, 1142], map.getMaxZoom()),{icon: markerZoom})
     .bindPopup('<b>markerHead_9');
 var markerHead_10 = L.marker(map.unproject([2051, 1088], map.getMaxZoom()),{icon: markerZoom})
//     .bindPopup('<b>markerHead_10');
// var markerHead_11 = L.marker(map.unproject([2072, 1159], map.getMaxZoom()),{icon: markerZoom})
//     .bindPopup('<b>markerHead_11');
// var markerHead_12 = L.marker(map.unproject([2112, 1133], map.getMaxZoom()),{icon: markerZoom})
//     .bindPopup('<b>markerHead_12');
// var markerHead_13 = L.marker(map.unproject([2119, 1105], map.getMaxZoom()),{icon: markerZoom})
//     .bindPopup('<b>markerHead_13');
// var markerHead_14 = L.marker(map.unproject([2091, 1057], map.getMaxZoom()),{icon: markerZoom})
//     .bindPopup('<b>markerHead_14');
// var markerHead_15 = L.marker(map.unproject([2122, 1033], map.getMaxZoom()),{icon: markerZoom})
//     .bindPopup('<b>markerHead_15');
// var markerHead_16 = L.marker(map.unproject([2031, 1218], map.getMaxZoom()),{icon: markerZoom})
//     .bindPopup('<b>markerHead_16');
// var markerHead_17 = L.marker(map.unproject([2150, 1550], map.getMaxZoom()),{icon: markerZoom})
//     .bindPopup('<b>markerHead_17');
// var markerHead_18 = L.marker(map.unproject([2070, 1254], map.getMaxZoom()),{icon: markerZoom})
//     .bindPopup('<b>markerHead_18');
// var markerHead_19 = L.marker(map.unproject([2083, 1298], map.getMaxZoom()),{icon: markerZoom})
//     .bindPopup('<b>markerHead_19');
// var markerHead_20 = L.marker(map.unproject([2155, 1279], map.getMaxZoom()),{icon: markerZoom})
//     .bindPopup('<b>markerHead_20');
// var markerHead_21 = L.marker(map.unproject([(1987, 1316], map.getMaxZoom()),{icon: markerZoom})
//     .bindPopup('<b>markerHead_21');
// var markerHead_22 = L.marker(map.unproject([1943, 1315], map.getMaxZoom()),{icon: markerZoom})
//     .bindPopup('<b>markerHead_22');
// var markerHead_23 = L.marker(map.unproject([2150, 1550], map.getMaxZoom()),{icon: markerZoom})
//     .bindPopup('<b>markerHead_23');

// var marker2 = L.marker(map.unproject([1966, 1800], map.getMaxZoom()),{icon: myIconRed})
//     .bindPopup('<b>marker2');
// var marker3 = L.marker(map.unproject([1714, 1956], map.getMaxZoom()),{icon: myIconRed})
//     .bindPopup('<b>marker3');
// var marker4 = L.marker(map.unproject([2304, 1252], map.getMaxZoom()),{icon: myIconGreen})
//     .bindPopup('<b>marker4');
// var marker5 = L.marker(map.unproject([1660, 1436], map.getMaxZoom()),{icon: myIconGreen})
//     .bindPopup('<b>marker5');
// var marker6 = L.marker(map.unproject([1636, 2110], map.getMaxZoom()),{icon: myIconGreen})
//     .bindPopup('<b>marker6');


    // Layer Groups
var lg_units = L.layerGroup([
    markerHead_1,
    markerHead_2,
    markerHead_3,
    markerHead_4,
    markerHead_5,
    markerHead_6,
    markerHead_7,
    markerHead_8,
    markerHead_9,
    markerHead_10,
    // markerHead_11,
    // markerHead_12,
    // markerHead_13,
    // markerHead_14,
    // markerHead_15,
    // markerHead_16,
    // markerHead_17,
    // markerHead_18,
    // markerHead_19,
    // markerHead_20,
    // markerHead_21,
    // markerHead_22,
    // markerHead_23
]).addTo(map);
//var lg_locat = L.layerGroup([marker4,marker5,marker6]).addTo(map);

var overlays = {
    "Creatures" : lg_units,
    "Locations" : lg_locat,
}

 // Add Layer Control
L.control.layers(null, overlays).addTo(map);










var geojson = L.geoJson(geojsonSample, {

style: function (feature) {
    return {color: feature.properties.color};
},

onEachFeature: function (feature, layer) {
    var popupText = 'geometry type: ' + feature.geometry.type;

    if (feature.properties.color) {
        popupText += '<br/>color: ' + feature.properties.color;
    }

    layer.bindPopup(popupText);
}
});

geojson.addLayer(new L.Marker(new L.LatLng(32.249974, 24.082031)));
geojson.addLayer(new L.Marker(new L.LatLng(22.593726, 38.671875)));

var eye11 = new L.Marker(new L.LatLng(26.902477, 67.851563));
var eye21 = new L.Marker(new L.LatLng(15.623037, 68.554688));
var eye31 = new L.Marker(new L.LatLng(22.593726, 38.671875));
var nose1 = new L.Marker(new L.LatLng(22.593726, 38.671875));
var nosed = new L.Marker(new L.LatLng(22.593726, 38.671875));



var markers = L.markerClusterGroup();
markers.addLayer(geojson).addLayers([eye11,eye21,eye31,nose1]);

map.addLayer(markers);
