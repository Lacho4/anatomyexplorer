var mapSW = [0, 4096],
  mapNE = [4096, 0];
// Declare map Object
var map = L.map("map").setView([0, 0], 2);

var zoomHome = L.Control.zoomHome();
zoomHome.addTo(map);

/*
 *
 *  Markers & Content are matched by:
 *
 *   ALT: Image on the Map
 *   ID: The title of the muscle - sidebar
 *   CLASS: The Description of the muscle - sidebar2
 *
 *
 */




// Reference the Tiles

// Desktop

if (window.screen.width < 768) {
  L.tileLayer("maps/{z}/{x}/{y}.png", {
    minZoom: 0,
    maxZoom: 4,
    continousWorld: false,
    noWrap: true,
    crs: L.CRS.Simple,
  }).addTo(map);

  //max granici na mapata
  map.setMaxBounds(
    new L.LatLngBounds(
      map.unproject(mapSW, map.getMaxZoom()),
      map.unproject(mapNE, map.getMaxZoom())
    )
  );
}
else {
  L.tileLayer("maps/{z}/{x}/{y}.png", {
    minZoom: 0,
    maxZoom: 4,
    continousWorld: false,
    noWrap: true,
    crs: L.CRS.Simple,
  }).addTo(map);

  //max granici na mapata
  map.setMaxBounds(
    new L.LatLngBounds(
      map.unproject(mapSW, map.getMaxZoom()),
      map.unproject(mapNE, map.getMaxZoom())
    )
  );
}

document.getElementsByClassName(
  "leaflet-control-attribution"
)[0].style.display = "none";

//   console.log("ghjghj!");

// Icons
var myIconRed = L.icon({
  iconUrl: "scripts/leafletJS/images/leaf-red.png",
  iconSize: [38, 95],
  iconAnchor: [22, 94],
  popupAnchor: [-3, -76],
  shadowUrl: "scripts/leafletJS/images/leaf-shadow.png",
  shadowSize: [41, 41],
  shadowAnchor: [5, 40],
});
//L.marker([50.505, 30.57], {icon: myIconRed}).addTo(map);
var myIconGreen = L.icon({
  iconUrl: "scripts/leafletJS/images/leaf-green.png",
  iconSize: [38, 95],
  iconAnchor: [22, 94],
  popupAnchor: [-3, -76],
  shadowUrl: "scripts/leafletJS/images/leaf-shadow.png",
  shadowSize: [41, 41],
  shadowAnchor: [5, 40],
});
var markerZoom = L.icon({
  iconUrl: "img/marker-mine.svg",
  iconSize: [28],
  iconAnchor: [14, 14],
  // popupAnchor: [-3, -76],
  //  shadowUrl: 'scripts/leafletJS/images/leaf-shadow.png',
  // shadowSize: [41, 41],
  // shadowAnchor: [5, 40]
});
var markerZoomHover = L.icon({
  iconUrl: "img/marker-mine-hover.svg",
  iconSize: [28],
  iconAnchor: [14, 14],
  // popupAnchor: [-3, -76],
  //  shadowUrl: 'scripts/leafletJS/images/leaf-shadow.png',
  // shadowSize: [41, 41],
  // shadowAnchor: [5, 40]
});
var markerZoomSVG = L.icon({
  iconUrl: "img/marker-mine.svg",
  iconSize: [25, 25],
  iconAnchor: [14, 14],
});
const fontAwesomeIcon = L.divIcon({
  html: '<i class="fas fa-search" style="color:green"></i>',
  iconSize: [40, 40],
  className: "myDivIcon",
});

// Markers and Popups
var refMarker = L.marker([2, -70], {
  draggable: true,
});
refMarker.bindPopup("");

// Pixels
var marker_center = L.marker(map.unproject([2048, 2048], map.getMaxZoom()));
marker_center.bindPopup("markerCenter").openPopup();

refMarker.on("dragend", function (e) {
  refMarker
    .getPopup()
    .setContent(
      // "Clicked " +
      refMarker.getLatLng().toString() +
        "<br />" +
        "Pixels " +
        map.project(refMarker.getLatLng(), map.getMaxZoom().toString())
    )
    .openOn(map);
});
//refMarker.addTo(map);

var randomNumber = 0;
var addressPointsHead = [
  [70.612614, -7.734375, "markerHead" + randomNumber++], // head 1
  [64.699105, -5.976563, "markerHead" + randomNumber++], // head 2
  [60.673179, 5.800781, "markerHead" + randomNumber++], // neck 1
  [57.797944, 4.570313, "markerHead" + randomNumber++], // neck 2
  [54.572062, 4.570313, "markerHead" + randomNumber++], // neck 3
  [52.802761, -25.664063, "markerHead" + randomNumber++], // levo ramo marker 1
  [49.724479, -27.070313, "markerHead" + randomNumber++], // levo ramo marker 2
  [32.324276, -30.849609, "markerHead" + randomNumber++], // levo raka marker 1
  [24.926295, -31.376953, "markerHead" + randomNumber++], // levo raka marker 2
  [24.046464, -28.212891, "markerHead" + randomNumber++], // levo raka marker 3
  [2.864255, -36.605469, "markerHead" + randomNumber++], //  levo dlanka marker 1
  [44.339565, 4.21875, "markerHead" + randomNumber++], //   gradi sredina
  [48.922499, 17.929688, "markerHead" + randomNumber++], // gradi desno
  [40.178873, 25.3125, "markerHead" + randomNumber++], //   desno biceps 1
  [40.780541, 28.476563, "markerHead" + randomNumber++], //  desno biceps 2
  [-1.230374, -16.347656, "markerHead" + randomNumber++], //levo noga 1
  [-18.479609, -14.765625, "markerHead" + randomNumber++], // levo noga 2
  [-36.597889, -15.820313, "markerHead" + randomNumber++], //levo noga 3
  [-44.21371, -17.402344, "markerHead" + randomNumber++], // levo noga 4
  [-45.95115, -9.84375, "markerHead" + randomNumber++], //   levo noga 5
  [-4.565474, 9.198438, "markerHead" + randomNumber++], //  desno noga 1
  [-23.241346, 4.570313, "markerHead" + randomNumber++], //  desno noga 2
  [-51.508742, 15.820313, "markerHead" + randomNumber++], //  desno noga 3
  [-54.059388, 9.492188, "markerHead" + randomNumber++], //  desno noga 4
  [-58.447733, 11.074219, "markerHead" + randomNumber++], //  desno noga 5
  [-58.722599, 13.535156, "markerHead" + randomNumber++], //  desno noga 6
  [-59.534318, 16.523438, "markerHead" + randomNumber++], //  desno noga 7
];

if (window.screen.width < 768) {
  var markersHead = L.markerClusterGroup({
    showCoverageOnHover: false,
    animate: true,
    spiderfyOnMaxZoom: false,
    disableClusteringAtZoom: 2,
    //maxClusterRadius:35
  });
} else {
  var markersHead = L.markerClusterGroup({
    showCoverageOnHover: false,
    animate: true,
    spiderfyOnMaxZoom: false,
    disableClusteringAtZoom: 3,
    //maxClusterRadius:35
  });
}

//$(document).ready(function () {
//    var sidebar2 = document.getElementById("sidebar2");
//    sidebar2.classList.add("collapsed");
//});


if (window.screen.width <= 768) {
  var allSidebarLinks = document.querySelectorAll('*[class^="markerHead"]');

  if (allSidebarLinks != undefined) {
    for (var i = 0; i < allSidebarLinks.length; i++) {
      currentSidebarLink = allSidebarLinks[i];

      var link_a = document.createElement("a");
      link_a.classList.add("btn");
      link_a.classList.add("btn-lg");
      link_a.classList.add("active");
      link_a.classList.add("kopce2");

      link_a.innerHTML = "View on Map >>";

      link_a.addEventListener("touch", function (e) {
        clickTheMarker(e.target.parentElement.className);
        setTimeout(() => {
          closeButton_mobile();
        }, 910);
      });
      link_a.onclick = function (e) {
        clickTheMarker(e.target.parentElement.className);
        setTimeout(() => {
          closeButton_mobile();
        }, 910);
      };

      currentSidebarLink.appendChild(link_a);
    }
  }
}

var allZooms = document.querySelectorAll(".leaflet-control-zoom");

if (allZooms != undefined) {
  allZooms[0].style.display = "none";
  // allZooms[1].style.display = "none";
}

//window.onresize = () => {
//  stickyImg();
//};

//window.onload = () => {
//  stickyImg();
//};

function stickyImg() {
  if (window.screen.width > 768) {
    var btnpos = document.querySelector(".btnpos");
    if (btnpos != undefined) {
      setTimeout(() => {
        var sidebarWidth = document.getElementById("sidebar");

        var width = sidebarWidth.offsetWidth + 20;
        btnpos.style.left = width + "px";
      }, 60);
    }
  }
}

function clickTheMarker(markerAlt) {

   //closeButton();
   if (window.screen.width <= 768) {
    zoomOutAfterHide();
}

  //closeButton();
  //var centerMap = document.querySelectorAll(".fa-dot-circle");
  //if (centerMap != undefined) {
  //  centerMap[0].click();
  //}

    setTimeout(() => {
   
    var allClusters = document.querySelectorAll(".marker-cluster");
    if (allClusters.length != 0) {
     // for (var h = 5; h < allClusters.length; h++) {
        var currentCluster = allClusters[5];
        currentCluster.click();
     // }
    }
    setTimeout(() => {
      var allMarkers = getImagesByAlt(markerAlt);
      var matchedMarker = allMarkers[0];

      if (matchedMarker != undefined) {
        matchedMarker.click();
        backToDefaultImgs();
        setTimeout(() => {
            matchedMarker.src = "img/marker-mine-hover.svg";
            //matchedMarker.style.width = "60px";
            //matchedMarker.style.height = "60px";
        }, 100);
          console.log("Marker found! \n");
         // if (!allOkay) location.reload();
      } else {
          console.log("No matching marker found on map!");
          location.reload();
      }
    }, 300);
  }, 600);
}

function backToDefaultImgs() {
  var allMarkers = document.querySelectorAll('*[alt^="markerHead"]');
  for (var l = 0; l < allMarkers.length; l++) {
    var currentMarker = allMarkers[l];
    currentMarker.src = "img/marker-mine.svg";
  }
}

function backToAllContent() {
  var sidebar = document.getElementById("sidebar");
  sidebar.classList.remove("collapsed");
  sidebar.style.display = "";
  var sidebar2 = document.getElementById("sidebar2");
  sidebar2.classList.add("collapsed");
  sidebar2.style.display = "none";

  var centerMap = document.querySelectorAll(".fa-dot-circle");
  if (centerMap != undefined) {
    centerMap[0].click();
  }
}

function closeButton() {
  var sidebar = document.getElementById("sidebar");
  sidebar.classList.add("collapsed");

  var sidebar2 = document.getElementById("sidebar2");
  sidebar2.classList.add("collapsed");

  // setTimeout(() => {
  //   sidebar.style.display = "none";
  //   sidebar2.style.display = "none";
  // }, 400);

  var centerMap = document.querySelectorAll(".fa-dot-circle");
  if (centerMap != undefined) {
    centerMap[0].click();
  }
  stickyImg();
}

function closeButton_mobile() {
  var sidebar = document.getElementById("sidebar");
  sidebar.classList.add("collapsed");

  var sidebar2 = document.getElementById("sidebar2");
  sidebar2.classList.add("collapsed");

  stickyImg();
}

function clickZoom(e) {
  map.setView(e.target.getLatLng(), 4);
}



/*   Head and Neck -->  Region  ::  Highlight  */

if (window.screen.width > 768) {
  setTimeout(() => {
    var head = document.querySelector(".headRegion");
    head.addEventListener("mouseover", function (e) {
      this.style.stroke = "green";
      var text = document.getElementById("headRegion");
      text.classList.add("markers_title_hovered");
    });
    head.addEventListener("mouseout", function (e) {
      this.style.stroke = "rgba(255,255,0,0)";
      var text = document.getElementById("headRegion");
      text.classList.remove("markers_title_hovered");
    });

    var chest = document.querySelector(".chestRegion");
    chest.addEventListener("mouseover", function (e) {
      this.style.stroke = "green";
      var text = document.getElementById("chestRegion");
      text.classList.add("markers_title_hovered");
    });
    chest.addEventListener("mouseout", function (e) {
      this.style.stroke = "rgba(255,255,0,0)";
      var text = document.getElementById("chestRegion");
      text.classList.remove("markers_title_hovered");
    });

    var abdomen = document.querySelector(".abdRegion");
    abdomen.addEventListener("mouseover", function (e) {
      this.style.stroke = "green";
      var text = document.getElementById("abdRegion");
      text.classList.add("markers_title_hovered");
    });
    abdomen.addEventListener("mouseout", function (e) {
      this.style.stroke = "rgba(255,255,0,0)";
      var text = document.getElementById("abdRegion");
      text.classList.remove("markers_title_hovered");
    });

    var arms = document.querySelector(".armsRegion");
    arms.addEventListener("mouseover", function (e) {
      this.style.stroke = "green";
      var text = document.getElementById("armsRegion");
      text.classList.add("markers_title_hovered");
    });
    arms.addEventListener("mouseout", function (e) {
      this.style.stroke = "rgba(255,255,0,0)";
      var text = document.getElementById("armsRegion");
      text.classList.remove("markers_title_hovered");
    });

    var legs = document.querySelector(".legsRegion");
    legs.addEventListener("mouseover", function (e) {
      this.style.stroke = "green";
      var text = document.getElementById("legsRegion");
      text.classList.add("markers_title_hovered");
    });
    legs.addEventListener("mouseout", function (e) {
      this.style.stroke = "rgba(255,255,0,0)";
      var text = document.getElementById("legsRegion");
      text.classList.remove("markers_title_hovered");
    });
  }, 2000);
}

/*   Head and Neck -->  Region  ::  Click  */

var headShown = 0;
var chestShown = 0;
var armsShown = 0;
var legsShown = 0;

window.onload = () => {
    hideAll();
};

function hideAll() {
    var allSidebarLinks = document.querySelectorAll('*[id^="markerHead"]');

    if (allSidebarLinks != undefined) {
        for (var lll in allSidebarLinks) {
            if (lll == "length") break;
            var link = allSidebarLinks[lll];

            link.hidden = true;            
        }
    }

    // Refresh the stack :D

    if (headShown > 5) location.reload();
    if (chestShown > 5) location.reload();
    if (armsShown > 5) location.reload();
    if (legsShown > 5) location.reload();
}

function zoomOutAfterHide() {
    setTimeout(() => {
        var lat = ["LatLng", "5.615986"];
        var lng = [" 0", ""];

        map.flyTo([lat[1], lng[0]], 2, {
            animate: true,
            duration: 0.3,
        });
    }, 100);   
}

function clickRegion(regionNum) {
    switch (regionNum) {
        case "0":
            hideAll();

            if (headShown % 2 == 0) {
                showSidebarLinks(".head");
                ++headShown;
            }
            else {
                zoomOutAfterHide();
                ++headShown;
            }

            var lat = ["LatLng", "71.187754"];
            var lng = [" 0", ""];

            map.flyTo([lat[1], lng[0]], 4, {
                animate: true,
                duration: 0.3,
            });
            break;
        case "1":
            hideAll();

            if (chestShown % 2 == 0) {
                showSidebarLinks(".chest");
                ++chestShown;
            }
            else {
                zoomOutAfterHide();
                ++chestShown;
            }

            var lat = ["LatLng", "32.842674"];
            var lng = [" 0", ""];

            map.flyTo([lat[1], lng[0]], 4, {
                animate: true,
                duration: 0.3,
            });
            break;
        case "2":
            //showHideSidebarLinks(".arms");   NEMET LINKOJ ZA OVIE MARA !!

            var lat = ["LatLng", "5.615986"];
            var lng = [" 0", ""];

            map.flyTo([lat[1], lng[0]], 4, {
                animate: true,
                duration: 0.3,
            });
            break;
        case "3":
            hideAll();

            if (armsShown % 2 == 0) {
                showSidebarLinks(".arms");
                ++armsShown;
            }
            else {
                zoomOutAfterHide();
                ++armsShown;
            }

            var lat = ["LatLng", "39.909736"];
            var lng = [" -28.125", ""];

            map.flyTo([lat[1], lng[0]], 4, {
                animate: true,
                duration: 0.3,
            });
            break;
        case "4":
            hideAll();

            if (legsShown % 2 == 0) {
                showSidebarLinks(".legs");
                ++legsShown;
            }
            else {
                zoomOutAfterHide();
                ++legsShown;
            }

            var lat = ["LatLng", "-37.439974"];
            var lng = [" 9.492188", ""];

            map.flyTo([lat[1], lng[0]], 4, {
                animate: true,
                duration: 0.3,
            });
            break;
        default:
    }
}

function showSidebarLinks(regionName) {
    var allLinks = document.querySelectorAll(regionName);

    if (allLinks != undefined) {
        for (var a in allLinks) {
            var link = allLinks[a];

            if (a == "length") break;

            link.hidden = false;
        }
    }
}

/*   Region -->  Head and Neck   */

function highlightRegion(regionName, toggle) {
  if (toggle == "show") {
    var region = document.querySelector("." + regionName);
    if (region != undefined) {
    region.style.stroke = "green";
  } 
  } else if (toggle == "hide") {
    var region = document.querySelector("." + regionName);
    if (region != undefined) {
    region.style.stroke = "rgba(255,255,0,0)";
  }
}
}

/*  Sidebar Links --> Markers    */

var allLinksOnsSidebar = document.querySelectorAll(".markers_title");

for (var z = 0; z < allLinksOnsSidebar.length; z++) {
    var currentLink = allLinksOnsSidebar[z];

    currentLink.addEventListener("touch", function (e) {
        clickTheMarker(e.target.id);
        console.log("Touch Event Works!");
    });
    currentLink.addEventListener("click", function (e) {
        clickTheMarker(e.target.id);
    });
    currentLink.addEventListener("mouseover", function (e) {
        var allMarkers = getImagesByAlt(e.target.id);
        var matchedMarker = allMarkers[0];

        if (matchedMarker != undefined) {
            matchedMarker.src = "img/marker-mine-hover.svg";
            matchedMarker.style.width = "28px";
            //matchedMarker.style.height = "28px";
        }
    });
    currentLink.addEventListener("mouseout", function (e) {
        var allMarkers = getImagesByAlt(e.target.id);
        var matchedMarker = allMarkers[0];

        if (matchedMarker != undefined) {
            matchedMarker.src = "img/marker-mine.svg";
            matchedMarker.style.width = "28px";
            // matchedMarker.style.height = "28px";
        }
    });
}

/*   Markers --> Sidebar Links  */

for (var i = 0; i < addressPointsHead.length; i++) {
  var a = addressPointsHead[i];
  var title = a[2];

  var marker = L.marker(new L.LatLng(a[0], a[1]), {
    alt: title,
    icon: markerZoom,
  })
    .on("mouseover", function (e) {
      var markerAlt = e.target.options.alt;
      var markerElement = document.getElementById(markerAlt);
      markerElement.classList.add("markers_title_hovered");
      e.target.setIcon(markerZoomHover);
    })
     //.on("click", clickZoom)
    .on("mouseout", function (e) {
      var markerAlt = e.target.options.alt;
      var markerElement = document.getElementById(markerAlt);
      markerElement.classList.remove("markers_title_hovered");
      e.target.setIcon(markerZoom);
    })
      .on("click", function (e) {
          showDesc(e);
      });
    marker.addEventListener("touch", function (e) {
        showDesc(e);
        console.log("Touch Event Works!");
    });
  //marker.bindPopup(title);
  markersHead.addLayer(marker);
}

function showDesc(e) {
    clickZoom(e);
    var sidebar = document.getElementById("sidebar");
    sidebar.classList.add("collapsed");
    sidebar.style.display = "none";
    var sidebar2 = document.getElementById("sidebar2");
    sidebar2.style.display = "";
    sidebar2.classList.remove("collapsed");

    var allMuscleDescriptions = document.querySelectorAll(
        '*[class^="markerHead"]'
    );
    var allMuscleDescriptions_Array = Array.from(allMuscleDescriptions);
    var markerAlt = e.target.options.alt;

    for (var l = 0; l < allMuscleDescriptions.length; l++) {
        var currentDesc = allMuscleDescriptions[l];

        if (currentDesc.classList.contains(markerAlt)) {
            currentDesc.style.display = "";
            console.log("All okay!!!");
        } else {
            currentDesc.style.display = "none";
        }
    }

    //e.target._popup = false;
    e.target.setIcon(markerZoom);
}

function openSidebar(sidebar) {}

map.addLayer(markersHead);

map.on("moveend", function (e) {
  //console.log("Hello world!");
});

function getImagesByAlt(alt) {
  var allImages = document.getElementsByTagName("img");
  var images = [];
  for (var i = 0, len = allImages.length; i < len; ++i) {
    if (allImages[i].alt == alt) {
      images.push(allImages[i]);
    }
  }
  return images;
}

function hovbeerrrr() {
  var allParagraph = document.querySelectorAll("");

  for (var i in allParagraph) {
    var currentElement = allParagraph[i];
    currentElement.style.display = "";
  }
}

function hideFirstParagraph() {
  var allParagraph = document.querySelectorAll(".even");

  for (var i in allParagraph) {
    var currentElement = allParagraph[i];
    currentElement.style.display = "none";
  }

  var allParagraph = document.querySelector("even");
  //firstParagraph.style.display = "none";
}

//marker1.addTo(map);

// =============== PanTo center marker

/*


var markerPrv = L.marker([32.842674, -64.6875], {
title: "marker_1",
})
.addTo(map)
.bindPopup("Marker 1")
.on("click", clickZoom);

var marker2 = L.marker([43.834527, -37.265625], {
title: "marker_2",
})
.addTo(map)
.bindPopup("Marker 2")
.on("click", clickZoom);

var marker3 = L.marker([11.178402, -19.6875], {
title: "marker_3",
})
.addTo(map)
.bindPopup("Marker 3")
.on("click", clickZoom);

function clickZoom(e) {
map.setView(e.target.getLatLng(), 15);
}

//everything below here controls interaction from outside the map
var markers = [];
markers.push(markerPrv);
markers.push(marker2);
markers.push(marker3);



function markerFunction(id) {
for (var i in markers) {
  var markerID = markers[i].options.title;
  var position = markers[i].getLatLng();
  if (markerID == id) {
    map.setView(position, 4); // second ARG- zoom lveel
    //markers[i].openPopup();
  }
}
}

$("a").click(function () {
markerFunction($(this)[0].id);
});
*/

var htmlStar = L.map("html-star", { scrollWheelZoom: false }).setView(
  [37.8, -96],
  4
);
L.tileLayer("http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png").addTo(
  htmlStar
);

L.easyButton('<span class="star">&starf;</span>', function () {
  alert("you just clicked the html entity &starf;");
}).addTo(htmlStar);

//var map = new L.map('map', { zoomControl: false });
